## My Task

Hello Cobiro :)

I tried to do my best with the time I had.

So I tried to do all the bonus points for the API part, but I hadn't have enough time to do the PHP and Docker Bonus points.

I commented my work the best I could.

And to answer the question : 10 vs 1.000.000 menu items - what would you do differently?

I think i would have created another way to count depth of a menu and the layer of an item by directly save it in the database and calculate it each time we have to (update, delete).

Also I would have added a limit to the get requests, by asking how many items the customer wants (-1 to have all). So it limits the size of the SQL requests.

I hope my work will interest you.

Best regards,

Karim Morel.




